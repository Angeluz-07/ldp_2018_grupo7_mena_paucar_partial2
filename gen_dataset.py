import random

#rand float with one decimal
def randVal(a, b):
    return round(random.uniform(a, b), 1)

#same as above but with two ranges
def randVal2(a, b, c, d):
    #for simplicity, i just will flip a coin.
    # if 1, use the first range. if 0, the other
    coin = random.randint(0, 1)
    firstRange = round(random.uniform(a, b), 1)
    secondRange = round(random.uniform(c, d), 1)
    return (firstRange if coin == 1 else secondRange)

def randVector():
		T = randVal(15, 31)
		H = randVal(39, 71)
		N = randVal(39, 51)
		return '1 {t} , 2 {h} , 3 {n} '.format(t=T,h=H,n=N)

#how many values will generate per class

#stands for Values Per Class/Category
vpc=50

with open('mydataset','w') as f:
		#Optimal -> 1
		for i in range(vpc):
				T = randVal(16, 30)
				H = randVal(40, 70)
				N = randVal(40, 50)
				f.write('{catg} 1:{t} 2:{h} 3:{n}\n'.format(catg=1,t=T,h=H,n=N))				

		#Inadecuate T ->2
		for i in range(vpc):
				T = randVal2(15, 15.9, 30.1, 31)
				H = randVal(40, 70)
				N = randVal(40, 50)
				f.write('{catg} 1:{t} 2:{h} 3:{n}\n'.format(catg=2,t=T,h=H,n=N))			

		#Inadecuate T and H->3
		for i in range(vpc):
				T = randVal2(15, 15.9, 30.1, 31)
				H = randVal2(39, 39.9, 70.1, 71)
				N = randVal(40, 50)
				f.write('{catg} 1:{t} 2:{h} 3:{n}\n'.format(catg=3,t=T,h=H,n=N))				

		#Inadecuate N ->4
		for i in range(vpc):
				T = randVal(16, 30)
				H = randVal(40, 70)
				N = randVal2(39, 39.9, 50.1, 51)
				f.write('{catg} 1:{t} 2:{h} 3:{n}\n'.format(catg=4,t=T,h=H,n=N))

"""
print("Now a random vector, so you can test:")
print(randVector())
"""

#about how to format the dataset file: https://stats.stackexchange.com/questions/61328/libsvm-data-format

