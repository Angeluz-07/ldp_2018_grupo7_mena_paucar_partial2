from http.server import BaseHTTPRequestHandler, HTTPServer
"""
Simple python server. Just for testing, not use in production.

run in linux terminal with:
	python3 myserver.py

stops with:
	CTRL+C

the server handles GET requests sending a random vector {T,H,N}

If is checked first by browser(i.e. Chrome) and then by the app, it will trouble.

if that hapens restart the server.


Ref.:
	https://github.com/howCodeORG/Simple-Python-Web-Server/blob/master/serv.py
"""

#--------utilities----------
import random 

#rand float with one decimal
def randVal(a, b):
    return round(random.uniform(a, b), 1)

def randVector():
		T = randVal(15, 31)
		H = randVal(39, 71)
		N = randVal(39, 51)
		return '{{1 {t} , 2 {h} , 3 {n} }}'.format(t=T,h=H,n=N)


#----------server----------------
PORT=8000
class Serv(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)        
        self.end_headers()
        self.wfile.write(bytes(randVector(), 'utf-8'))#I send  random vector as response

httpd = HTTPServer(('localhost', PORT), Serv)
httpd.serve_forever()



