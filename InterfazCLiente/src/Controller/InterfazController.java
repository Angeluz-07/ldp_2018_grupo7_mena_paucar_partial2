package Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;




public class InterfazController implements Initializable{
	
	private static final String USER_AGENT = "Mozilla/5.0";
    private static final String SERVER_PATH = "http://localhost/lenguajes/";
	
	
	@FXML
	private TextArea txtAreaTemperatura;
	@FXML
	private TextArea txtAreaHumedad;
	@FXML
	private TextArea txtAreaRuido;
	@FXML
	private TextArea txtAreaCategoria;
	@FXML
	private TextArea idMensaje;
	@FXML
	private Button idBoton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	private void interfaz(ActionEvent event) throws InterruptedException {
		// Se obtiene el Json
		//do {
			
			String json = getJSON();
			Object jsonObject =JSONValue.parse(json.toString());
			JSONObject jsonOb = (JSONObject) jsonObject;
			String categoria = (String)jsonOb.get("categoria");
			String temperatura = (String)jsonOb.get("temperatura");
			String humedad = (String)jsonOb.get("humedad");
			String ruido = (String)jsonOb.get("ruido");
			
			
			if(Integer.parseInt(categoria)==1) {
				idMensaje.setText("****----Optima----****");
			}else if(Integer.parseInt(categoria)==2) {
				idMensaje.setText(mensajeCat2());
			}else if(Integer.parseInt(categoria)==3) {
				idMensaje.setText(mensajeCat3());
			}else if(Integer.parseInt(categoria)==4) {
				idMensaje.setText(mensajeCat3());
			}
			
			txtAreaCategoria.setText(categoria);
			txtAreaTemperatura.setText(temperatura);
			txtAreaHumedad.setText(humedad);
			txtAreaRuido.setText(ruido);
			System.out.println(txtAreaHumedad.getText());
			

	}
	
	private String mensajeCat1() {					
		int numero = (int) (Math.random() * 3) + 1;
		String mensaje1= "Estimados colaboradores, se les recomienda \n tomar un descanso para mantenerse \n hidratados";
		String mensaje2="Estimados colaboradores, si llevan puesto \n traje pueden quitarse la chaqueta y aflojarse \n la corbata, consiguiendo as� mayor confort";
		String mensaje3="Estimados colaboradores, si su ubicaci�n de \n trabajo queda cerca de la ventana, \n se recomienda cerrar las persianas para \n protegerse de los rayos UV";
		
		switch(numero) {
		case 1:
			return mensaje1;
		case 2:
			return mensaje2;
		default:
			return mensaje3;
		}
	}
	
	private String mensajeCat2() {
		int numero = (int) (Math.random() * 3) + 1;
		
		String mensaje1="Estimados colaboradores, el nivel actual de \n humedad es alto; por lo tanto se \n recomienda reportar a RRHH para tomar las \n debidas precauciones";
		String mensaje2="Estimados colaboradores, si sufre de algun \n sintoma de alergia recomendamos acercarse \n al departamento medico para solicitar \n pastillas respectivas";
		String mensaje3="Estimados colaboradores, se recomienda \n usar cremas hidratantes para evitar la \n ploriferaci�n de hongos en el ambiente \n debido a la alta humedad";
		
		switch(numero) {
		case 1:
			return mensaje1;
		case 2:
			return mensaje2;
		default:
			return mensaje3;
		}
	}
	
	
	private String mensajeCat3() {
		int numero = (int) (Math.random() * 3) + 1;
		
		String mensaje1="Estimados colaboradores, se ha detectado niveles altos de decibeles; por lo tanto se recomienda ubicarse en las salas de reuniones";
		String mensaje2="Estimados colaboradores, se recomienda utilizar los auriculares de cancelaci�n de ru�do";
		String mensaje3="Estimados colaboradores, se ha detectado niveles altos de decibeles; por lo tanto se recomienda evitar conversaciones continuas entre compa�eros y reservarlas a las zonas de descanso.";
		
		switch(numero) {
		case 1:
			return mensaje1;
		case 2:
			return mensaje2;
		default:
			return mensaje3;
		}
	}
	
	
	private static String getJSON(){
        
        StringBuffer response = null;
        
        try {
            //Generar la URL
            String url = SERVER_PATH+"parametros.json";
            //Creamos un nuevo objeto URL con la url donde pedir el JSON
            URL obj = new URL(url);
            //Creamos un objeto de conexi�n
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            //A�adimos la cabecera
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            // Enviamos la petici�n por POST
            con.setDoOutput(true);      
            //Capturamos la respuesta del servidor
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            //Mostramos la respuesta del servidor por consola
            System.out.println("Respuesta del servidor: "+response);
            System.out.println();
            //cerramos la conexi�n
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return response.toString();
    }


}
