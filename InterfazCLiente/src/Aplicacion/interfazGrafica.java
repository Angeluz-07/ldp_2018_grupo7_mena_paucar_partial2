package Aplicacion;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class interfazGrafica extends Application{
	private static Parent root;
    private static Scene scene;
    private static Stage stage;
	

	@Override
	public void start(Stage stage) throws Exception {
		interfazGrafica.stage = stage;

        root = FXMLLoader.load(getClass().getResource("/FXML/Interfaz.fxml"));

        scene = new Scene(root);

        interfazGrafica.stage.setScene(scene);
        interfazGrafica.stage.setTitle("Ingreso");
        interfazGrafica.stage.setResizable(false);
        interfazGrafica.stage.show();
		
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	public static void logout() throws Exception{
	        root = FXMLLoader.load(interfazGrafica.class.getResource("/FXML/Interfaz.fxml"));

	        scene = new Scene(root);

	        interfazGrafica.stage.setScene(scene);
	        interfazGrafica.stage.setTitle("Ingreso");
	        interfazGrafica.stage.centerOnScreen();
	        interfazGrafica.stage.show();
	 }

}
