from http.server import BaseHTTPRequestHandler, HTTPServer
"""
Simple python server. Just for testing, not use in production.

run in linux terminal with:
	python3 server_tojavaapp.py

stops with:
	CTRL+C

the server handles GET requests sending a random vector {T,H,N}

If is checked first by browser(i.e. Chrome) and then by the app, it will trouble.

if that hapens restart the server.


Ref.:
	https://github.com/howCodeORG/Simple-Python-Web-Server/blob/master/serv.py
	https://gist.github.com/bradmontgomery/2219997
"""

#--------utilities----------
import json

def proccess_data(post_data):
    post_data = post_data.decode('utf-8') #get data(bytes) as string
    with open('info.json','w') as f:
        f.write(post_data)
    print (post_data) # <-- Print post data
#----------server----------------
PORT=8001
class Serv(BaseHTTPRequestHandler):

    def do_GET(self):
        try:
            file_to_open=open(self.path[1:]).read()
            self.send_response(200)							
        except:
            file_to_open= "File not found"
            self.send_response(404)
        self.end_headers()
        self.wfile.write(bytes(file_to_open, 'utf-8'))

    def do_POST(self):
				# in this way we get the content of post request
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        proccess_data(post_data)        
        self.send_response(200)
        self.end_headers()

httpd = HTTPServer(('localhost', PORT), Serv)
httpd.serve_forever()



