import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class MyUtil
{
    public static void sayHello()
    {
        System.out.println("Hello again");
    }
		
		/*
			receives file path and return map with x,y,z coordinates as keys and their 
			respectives values in a list, as values of those keys.
			
			the catg atribute is to get specific coordinates from one category.
			refer to more info about catgs: info about dataset.txt

			e.g.:
				{x=[21.8,..., 21.6], y=[42.5,..., 46.3], z=[40.9, ..., 39.8]}
		*/
		public static Map<String,List<Double>> getCoordinatesOfCatg(String catg , String file)
    {
				String line;
				String x,y,z;		
				Scanner input;			
				/*	
					coordinatesOfCatg
					{x=[21.8,..., 21.6], y=[42.5,..., 46.3], z=[40.9, ..., 39.8]}
				*/
				Map<String,List<Double>> coordinatesOfCatg=new HashMap();
				List<Double> x_coords=new ArrayList();
				List<Double> y_coords=new ArrayList();
				List<Double> z_coords=new ArrayList();
				try{
		      input= new Scanner(new File(file));
						
					while(input.hasNext()){

						line=input.nextLine();						
						//line format -->"1 1:21.8 2:42.5 3:40.9\n"
						
						//if first char corresponds to catg, retrieve coordinates
						if(line.startsWith(catg)){ 
								x=line.substring(4,8);//substring(a,b) gets [a,b)
								y=line.substring(11,15);
								z=line.substring(18,22);						
								
								x_coords.add(Double.parseDouble(x));
								y_coords.add(Double.parseDouble(y));
					 		  z_coords.add(Double.parseDouble(z));
			
								//System.out.printf("%s -> %f %f %f\n",line,Double.parseDouble(x), Double.parseDouble(y),Double.parseDouble(z));							
						}	
					}								
						//System.out.println(x_coords);						
						//System.out.println(y_coords);						
						//System.out.println(z_coords);
						coordinatesOfCatg.put("x",x_coords);
						coordinatesOfCatg.put("y",y_coords);
						coordinatesOfCatg.put("z",z_coords);
						
						//print result
						//System.out.println(coordinatesOfCatg);
				}catch(Exception e){
						System.out.println(e);
				}
			
			return coordinatesOfCatg;
    }
}
