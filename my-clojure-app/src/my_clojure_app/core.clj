(ns my-clojure-app.core
  (:gen-class)
  (:require [clj-http.client :as client]))



(use 'svm.core)

;;jutsu lib to graph
(use '[jutsu.core :as j])

;;lib to make post http request
;;(use '[clj-http.client :as client]))

;;java libs
(import java.util.Scanner)
(import java.io.File)
(import java.net.URL)
(import java.util.ArrayList)
(import java.util.List)
(import java.util.HashMap)
(import java.util.Map)

;;import java classes
(import MyUtil)


;;----------function to graphics----------

;;take name and map like {"x"=[21.8,..., 21.6], "y"=[42.5,..., 46.3], "z"=[40.9, ..., 39.8]}
(defn make-scatter-graph3d 
	 [name coordinates]
		{:x (get coordinates "x")
		 :y (get coordinates "y")	
		 :z (get coordinates "z")
		 :mode "markers"
		 :type "scatter3d"
		 :name name}
)

;;wrapper function for better readability
(defn get-coord-by-catg
	[catg]
	;;returns a map like {"x"=[21.8,..., 21.6], "y"=[42.5,..., 46.3], "z"=[40.9, ..., 39.8]}
	(MyUtil/getCoordinatesOfCatg catg "resources/mydataset")
)

;;parse vector received from external server
(defn parse-vector
		;;parse {1 26.2, 2 54.4, 3 42.4} as {"x"=[26.2], "y"=[54.4], "z"=[42.4]}
		[vector]
		{
			"x" [(get vector 1)],
			"y" [(get vector 2)],
			"z" [(get vector 3)]
		}
)

;;----------main----------
(defn -main
  "I don't do a whole lot ... yet."
  [& args]

	;; on using read-dataset put the file in resources folder and use the path as below
	(def dataset (read-dataset "resources/mydataset"))

	;;training	
  (println "Welcome.")	

	(def model (train-model dataset))
  (println "Training completed.")

	;;can use the first vector to test but...
	;;(def feature (last (first dataset)))

	;;let's use a random one, generated previously
	;;the vector is represented as a map.
	;;(def feature {1 15.7 , 2 50.0 , 3 50.0})
	
	
	;;the previous vector is catg=2
	;;on predict we should confirm that
	;;but don't forget the model has an error asociated and may fail with other vectors.
	;;(println "\n" "catg predicted=" (predict model feature))

	(j/start-jutsu! 7000 true) ;;launch tab in browser to plot dots, using jutsu lib
	;;if throws errors, open a fresh clean browser session and restart program
	(Thread/sleep 5000) ;;for now it works by waiting and then plot the graph
	

  (j/graph! "scatter-3d" [(make-scatter-graph3d "Optimal" (get-coord-by-catg "1"))
								             (make-scatter-graph3d "Inadecuate T" (get-coord-by-catg "2"))	
														 (make-scatter-graph3d "Inadecuate TH" (get-coord-by-catg "3"))
														 (make-scatter-graph3d "Inadecuate N" (get-coord-by-catg "4"))])
	(println "First graph loaded...")
	

	(Thread/sleep 10000)
	(def myserver "http://127.0.0.1:8000/");; the server that provides the random vector

	(future
		(while true
			(Thread/sleep 60000)	
			;;slurp function makes get and returns content
			;;must implement a handler in case server is not active
			(def vectorReceived (read-string (slurp myserver))) ;read-string parse string to map data-structure

			(println  "Vector received = " vectorReceived )
			(println  "Category predicted = " (predict model vectorReceived))				
			(j/graph! "scatter-3d" [ 
																	 (make-scatter-graph3d "Optimal" (get-coord-by-catg "1"))
														       (make-scatter-graph3d "Inadecuate T" (get-coord-by-catg "2"))	
																	 (make-scatter-graph3d "Inadecuate TH" (get-coord-by-catg "3"))
																	 (make-scatter-graph3d "Inadecuate N" (get-coord-by-catg "4"))
																	 (make-scatter-graph3d "Vector received" (parse-vector vectorReceived))
																])			
			(println "Vector loaded on graph.")
			
			(def post-data  {:categoria  (predict model vectorReceived)
											 :vector {:temperatura (get vectorReceived 1)
																:humedad    (get vectorReceived 2)
																:ruido     (get vectorReceived 3)
															 }
											})			
			(client/post "http://127.0.0.1:8001/" {:form-params post-data :content-type :json})
			(println "Info sent to server_tojavaapp.py")
			
		)
	)			

)
