(defproject my-clojure-app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
								[svm-clj "0.1.3"]							
								[hswick/jutsu "0.1.1"]
								[clj-http "3.9.1"]
								[cheshire "5.8.0"]
								[http-kit "2.3.0-beta2"]];a workaround to make jutsu works
  :main ^:skip-aot my-clojure-app.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}

	;;makes reference to java source code
	:java-source-paths ["src/java"])


;;jutsu tutorial https://steemit.com/programming/@hswick/simple-interactive-graphs-with-clojure
